<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



//The Code is documentation enough : P

///////////////////////////////////////////////////////////////////// URL Stuffs
Route::get('/', function()
{
    return Redirect::to('home');
});

Route::get('friends', function()
{
	return View::make('social.friends');
});
Route::get('home', function()
{
	$threads = get_threads();
	return View::make('social.home')->withThreads($threads);
});

Route::get('update/{id}', function($id)
{
	$thread = get_thread($id);
	return View::make('social.update')->withThread($thread);
});
Route::get('deletePost/{id}', function($id)
{

	$sql = "delete from threads where threads.id = ?";
	DB::delete($sql,(array)$id);
	$sql = "delete from comments where comments.threadId = ?";
	DB::delete($sql,(array)$id);
    return Redirect::to(url("home"));
});
Route::get('deleteComment/{id}', function($id)
{

	$sql = "delete from comments where comments.id = ?";
	DB::delete($sql,(array)$id);
    return Redirect::to(url("home"));

});
////////////////////////////////////////////////////////////////// EndOfUrlStuff


//////////////////////////////////////////////////////////////// Database stuffs
// getters
// Retrieve data from database according to specific needs($id)
function get_threads()
{
  $sql = "select * from threads order by id DESC";
  $threads = DB::select($sql);
  return $threads;
}
function get_thread($id)
{
	$sql = "select id, title, name, post, filename from threads where id = ?";
	$threads = DB::select($sql, array($id));
// If we get more than one item or no items display an error
	if (count($threads) != 1) 
	{
    die("Invalid query or result: $sql\n");
  	}
	// Extract the first item (which should be the only item)
	$thread = $threads[0];
	return $thread;
}
function get_comments($id){
	$sql = "select * from comments where comments.threadId = ?";
	$comments = DB::select($sql, array($id));
	return $comments;
}



// setters
// Save data to database according to ($id)
Route::post('add_item_action', function()
{
	$name = Input::get('name');
	$post = Input::get('post');
	$title = Input::get('title');
	$file = Input::file('image');
	$destinationPath = 'images/';
	$filename="";
	if ($file){
		$filename = $file->getClientOriginalName();
		Input::file('image')->move($destinationPath, $filename);
	} 
	$id = add_item($title, $name, $post, $filename);
	// If successfully created then display newly created item
	if ($id) 
	{
	    return Redirect::to(url("home"));
	} 
	else
	{
		die("Error adding item");
	}
});
function add_item($title, $name, $post, $filename) 
{
	$sql = "insert into threads (title, name, post, filename) values (?, ?, ?, ?)";
	
	DB::insert($sql, array($title, $name, $post, $filename));
	
	$id = DB::getPdo()->lastInsertId();
	
	return $id;
}
Route::post('update_item_action', function()
{
	$id = Input::get('id');
	$name = Input::get('name');
	$post = Input::get('post');
	$title = Input::get('title');
	$file = Input::file('image');
	$destinationPath = 'images/';
	$filename=Input::get('filename');
	if ($file){
		$filename = $file->getClientOriginalName();
		Input::file('image')->move($destinationPath, $filename);
	} 
	$sql = "update threads set name = ?, post = ?, title = ?, filename = ? where id = ?";
	DB::update($sql, array($name, $post, $title, $filename, $id));
    return Redirect::to(url("home"));
});
Route::post('add_comment_action', function()
{
	$name = Input::get('name');
	$message = Input::get('message');
	$threadId = Input::get('threadId');
	$file = Input::file('image');
	$destinationPath = 'images/';
	$filename="";
	if ($file){
		$filename = $file->getClientOriginalName();
		Input::file('image')->move($destinationPath, $filename);
	} 
	$id = add_comment($name, $message, $threadId, $filename);
	// If successfully created then display newly created item
	if ($id) 
	{
	    return Redirect::to(url("home"));
	} 
	else
	{
		die("Error adding item");
	}
});
function add_comment($name, $message, $threadId, $filename) 
{
	$sql = "insert into comments (name, comment, threadId , filename) values (?, ?, ?, ?)";
	
	DB::insert($sql, array($name, $message, $threadId, $filename));
	
	$id = DB::getPdo()->lastInsertId();
	
	return $id;
}
// endOfGetters
///////////////////////////////////////////////////////////// endOfDatabaseStuff