drop table if exists threads;
drop table if exists comments;

create table threads (
    id integer not null primary key autoincrement,
    title varchar(80) not null,
    name varchar(80) not null,
    post text default '',
    filename text default ''
);
create table comments(
    id integer not null primary key autoincrement,
	threadId INTEGER NOT NULL REFERENCES threads(id),
    name varchar(80) not null,
    comment text default '',
    filename text default ''
);