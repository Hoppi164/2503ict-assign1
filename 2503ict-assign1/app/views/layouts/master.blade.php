<!DOCTYPE html>
<html>

<head>
    <title>Triple Asterix</title>
    <link rel="icon" type="image/ico" href="/2503ict-assign1/public/favicon.ico" />

</head>

<body>
    <!--NAVBAR-->
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class = "container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only"> Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/2503ict-assign1/public/home"><span class="glyphicon glyphicon-asterisk"></span><span class="glyphicon glyphicon-asterisk"></span><span class="glyphicon glyphicon-asterisk"></span> TRIPLE ASTERIX <span class="glyphicon glyphicon-asterisk"></span><span class="glyphicon glyphicon-asterisk"></span><span class="glyphicon glyphicon-asterisk"></span></a>
            </div>
            <!--End of Header-->
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/2503ict-assign1/public/docs/erDiagram.png">ER Diagram</a></li>
                    <li><a href="/2503ict-assign1/public/docs/siteDiagram.png">Site Diagram</a></li>
                    <li><a href="/2503ict-assign1/public/docs/documentation.pdf">Documentation</a></li>
                    <li><a href="/2503ict-assign1/public/friends">Profile</a></li>
                    <li id = "navBtn1"><a href="/2503ict-assign1/public/home">Home</a></li>
                </ul>
            </div>
            <!--End of Nav Body-->
        </div>
    </nav>
    <!-- NAVBAR -->
    
    <div class="row" >
           <div class="col-md-3 hidden-sm hidden-xs">
                @yield('left')
            </div>
              
            <div class="col-md-7"> 
                @yield('mid')
            </div>
              
            <div class="col-md-2"> 
                @yield('right')
            </div>
  
      </div>


    <!--MetaTags-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{ HTML::script('jquery-2.1.3.min.js') }}
    {{ HTML::style('bootstrap.min.css') }}
    {{ HTML::style('css/stylesheet.less') }}

    <script type="text/javascript" src="bootstrap.min.js"></script>
    

    </body>

</html>
