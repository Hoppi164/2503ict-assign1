@extends('layouts.master')

@section('left')
<!--CreatePost Form-->
<div class="panel panel-info newPost">
    <div class="panel-heading">
        <div class="panel-title">Make Post</div>
    </div>     
    <div class="panel-body well well-sm" >
        <form class="form-horizontal" role="form" method="post" action="add_item_action" enctype="multipart/form-data">
            <div class="inputGroup">
                <label class="control-label" for="name">Enter Post Title:</label>
                <input type="text" required="true" class="form-control" name="title" id="title" value="" placeholder="My Awesome Post">                                        
                <label class="control-label" for="name">Enter Name:</label>
                <input type="text" required="true" class="form-control" name="name" id="name" value="" placeholder="My Name Is">                                        
                <label class="control-label" for="post">Enter Message:</label>
                <textarea class="form-control" required="true" rows="4"  name="post" id="post" placeholder="What i think.."></textarea> 
                <br>
                <input class="form-control" type="file" name="image" id="image" />
                <br>
                <input class="form-control" type="submit" value="Submit"/>
            </div>
        </form>
    </div>
</div>
<!--endCreatePostForm-->
@stop


@section('mid')

@if ($threads)

@foreach ($threads as $thread)

    <!--POST-->
    <div class = "panel panel-primary">
        <div class="panel-heading">{{{ $thread->title }}}</div>
        <div class="post">
            <div class = "col-md-2">
                <a href="#" class="thumbnail" ><img src="{{{ url('images/smile.png') }}}" class = "displayPic"></a>
                <p class = "name"> {{{ $thread->name }}}</p>
            </div>
            <div class = "col-md-10 panel">
                <p class = "message"> {{{ $thread->post }}} </p>
                <a class="thumbnail" href="#" >{{ HTML::image("images/$thread->filename", '  No Image :  (  ') }}</a>
            </div>
        </div>

        <!--COMMENTS-->
        <div class="panel-group" id="accordion">
            <div class = "panel panel-info">
                <div class="panel-heading showBtn"><h4 class="panel-title"><a data-toggle="collapse" data-target="#commentForm{{{ $thread->id }}}" class = "collapsed">New Comment</a></h4></div>
                    <div id="commentForm{{{ $thread->id }}}" class="panel-collapse collapse">
                        <div class="panel-body comment-body">
                                <!--Comment Form-->
                            <form method="post" role="form" action="add_comment_action" class="form-horizontal" enctype="multipart/form-data">
                                <div class="inputGroup">
                                    <input type="hidden" name="threadId" value="{{{ $thread->id }}}"> 
                                    <label class="control-label" for="name">Enter Name:</label>
                                    <input type="text" required="true" class="form-control" name="name" id="name" value="" placeholder="My Name Is">                                        
                                    <label class="control-label" for="message">Enter Message:</label>
                                    <textarea class="form-control" required="true" rows="4"  name="message" id="message" placeholder="What i think.."></textarea> 
                                    <br>
                                    <input class="form-control" type="file" name="image" id="image" />
                                    <br>
                                    <input class="form-control btn btn-success" type="submit" value="Submit"/>
                                </div>
                            </form>                                    
                                <!--End of comment form-->
                        </div>
                    </div>
                </div>
            <div class = "panel panel-default">
                <div class="panel-heading showBtn"><h4 class="panel-title"><a data-toggle="collapse" data-target="#collapseContainer{{{ $thread->id }}}"  class = "collapsed">View Comments ( {{{count(get_comments($thread->id))}}} )</a></h4></div>
                <div id="collapseContainer{{{ $thread->id }}}" class="panel-collapse collapse">
                <!--Comments Go in here-->
                    <div class="panel panel-default" id="comment">
                        
                        @foreach(get_comments($thread->id) as $comment)
                        <div class="panel panel-default" >
                        <div class ="panel-heading">
                            {{{$comment->name}}}
                        </div>
                        
                            <div class="panel panel-body commentBody">
                                <p class="col-md-6">{{{ $comment->comment}}}</p>
                                <a class="col-md-6 thumbnail" href="#" >{{ HTML::image("images/$comment->filename", '  No Image :  (  ') }}</a>
                            </div>
                        <a class="btn btn-danger" href="{{{ url("deleteComment/$comment->id") }}}">Remove Comment</a>
                        </div>
                        
                        @endforeach
                    </div>
                <!--Comments Go in here-->
                </div>
            </div>
        </div>
        <a class="btn btn-danger" href="{{{ url("deletePost/$thread->id") }}}">Delete Post</a>
        <a class="btn btn-success" href="{{{ url("update/$thread->id") }}}">Edit Post</a>
    </div>
    <!--END POST-->
@endforeach
@else
<p>No items found.</p>
@endif
@stop