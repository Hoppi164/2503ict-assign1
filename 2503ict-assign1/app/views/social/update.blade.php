@extends('layouts.master')



@section('mid')
<!--updateForm-->
<div class = "panel panel-primary">
    <form method="post" action="{{{ url('update_item_action') }}}" enctype="multipart/form-data">
        <input type="hidden" name="id" value="{{{ $thread->id }}}"> 
        <input type="hidden" name="filename" value="{{{ $thread->filename }}}"> 
        <div class="panel-heading">
            <input type="text" class="form-control" name="title" id="title" value="{{{ $thread->title }}}">
        </div>
        
        <div class="post">
            <div class = "col-md-2">
                <a href="#" class="thumbnail" ><img src="{{{ url("images/smile.png") }}}" class = "displayPic"></a>
                <p class = "name"> {{{ $thread->name }}}</p> 
                <!--NameVariable is viewable but not editable-->
                <input type="hidden" class="form-control" name="name" id="name" value="{{{ $thread->name }}}">                                        
            </div>
            <div class = "col-md-10 panel">
                <textarea class="form-control" rows="4"  name="post" id="post">{{{ $thread->post }}}</textarea>
                <input class="form-control" type="file" name="image" id="image" value=""> 
            </div>
        <a href="/2503ict-assign1/public/home" class ="btn btn-danger">Cancel</a>
        <input class="btn btn-success" type="submit" value="Submit"/>
        </div>
    </form>
</div>




@stop
