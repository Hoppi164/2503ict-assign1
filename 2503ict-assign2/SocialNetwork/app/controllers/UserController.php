<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()//LOGIN INDEX
	{
		return View::make('user.login');
	}

	public function edit()//EDIT PAGE
	{
		return View::make('user.edit');
	}
	public function profile($id){//PROFILE PAGE (PASSED IN ID)
		$user = get_UserDATA($id)[0];
		return View::make('user.profile')->with('user', $user); 
	}
	public function unfriend($id){//FUNCTION TO UNFRIEND SOMEONE
		$user = get_UserDATA($id)[0];
		$sql = "DELETE FROM friends WHERE adderID = ? AND addedID = ? OR adderID = ? AND addedID = ?";
		DB::delete($sql,[Auth::User()->id, $id, $id, Auth::User()->id]);
		return Redirect::to(URL::previous());
	}

	public function create()//CREATE NEW USER PAGE
	{
		return View::make('user.create'); 
		//
	}
	public function store()//ADDS NEW USER ACTION
	{
		$input = Input::all();
		$password = $input['password'];
		$encrypted = Hash::make($password);

		$user = new User;
		$user->username = $input['username'];
		$user->password = $encrypted;
		$user->email = $input['email'];
		$user->profileImage = "smile.png";
		$user->DOB = $input['DOB'];

		$user->save();
		return Redirect::route('user.index')->withInput()->with('created', $input['username']); 
		//
	}

	public function login(){//ATTEMPS TO LOGIN USER, REDIRECTS
		$userdata = array(
			'username' => Input::get('username'),
			'password' => Input::get('password'),
			);
		//authenticate
		if (Auth::attempt($userdata)){
			$username = Input::get('username');
			$ID = get_ID($username);
			// $ID = 1;
		    $threads = get_threads($ID);
		    return Redirect::to(url("home"));
		}
		else{
			return Redirect::to(URL::previous())->withInput();
		}
		
		
	}
	public function logout(){//LOGOUT
		Auth::logout();
		return Redirect::route('user.index');
	}

	public function search(){
		$query = Input::get('searchBar');
		$query = "%$query%";
		$sql = "select username, id from users where username like ?";
		$users = DB::select($sql, array($query));
		return Redirect::to(URL::previous())->withUsers($users);
	}
}
