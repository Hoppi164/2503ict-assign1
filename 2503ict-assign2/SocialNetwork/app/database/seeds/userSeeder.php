<?php

class userSeeder extends Seeder {

	public function run()
	{
        $newUser = new User;
        $newUser->username = "bob";
        $newUser->password = "bob";
        $newUser->DOB = "31/08/1994";
        $newUser->save();

        $newUser = new User;
        $newUser->username = "tom";
        $newUser->password = "tom";
        $newUser->DOB = "17/10/1996";
        $newUser->save();
	}
}