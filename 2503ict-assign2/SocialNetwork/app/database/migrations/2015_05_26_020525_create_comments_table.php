<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	public function up()
	{
		Schema::create('comments',
		function($table){
			$table->increments('id');
			$table->integer('threadId')->unsigned();
			$table->foreign('threadId')->references('id')->on('threads');
			$table->integer('userID')->unsigned();
			$table->foreign('userID')->references('id')->on('users');
			$table->string('comment');
			$table->string('filename');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comments');
	}

}
