<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('friends',
		function($table){
			$table->increments('id');
			$table->integer('adderID')->unsigned();
			$table->foreign('adderID')->references('id')->on('users');

			$table->integer('addedID')->unsigned();
			$table->foreign('addedID')->references('id')->on('users');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('friends');
	}

}
