<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThreadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('threads',
		function($table){
			$table->increments('id');
			$table->integer('userID')->unsigned();
			$table->foreign('userID')->references('id')->on('users');
			$table->string('title');
			$table->string('post');
			$table->string('filename');
			$table->integer('privacy');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('threads');
	}

}
