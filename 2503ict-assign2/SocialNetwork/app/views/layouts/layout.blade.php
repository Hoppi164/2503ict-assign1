<!DOCTYPE html>

<html>
    <head>
        <title>LOGIN TEST</title>

    
        <script>
        
        
                function search(str) {
                    showResults();
                    if (str.length==0) {
                        document.getElementById('results').innerHTML="<h4 class='match'>No Results Found</h4>";
                        return;
                    }
                    else{
                        document.getElementById('results').innerHTML="<h4 class='match'>Searching for : " + str + "</h4>";
                        document.getElementById('results').innerHTML+="<h4 class='match'> Press enter to see results. </h4>";
                        return;
                    }
                }
                function showResults(){
                    document.getElementById('results').style="display:block";
                    return;
                }
                function checkResults(){
                    var check = document.getElementById('results');
                    if(check !=''){
                        showResults();
                    }
                    alert(check.innerHTML);
                    return;
                }
        </script>
    </head>
    
    
    <body id="matx">

        <nav class = "navbar navbar-default navbar-static-top topbar">
            
            
                @if (Auth::Check())
                <form class = "navbar-form navbar-right">
                    <a href="{{{secure_url('home')}}}" class = "btn btn-default">Home</a>
                    <a href="{{{secure_url('user/edit')}}}" class = "btn btn-default">Profile</a>
                    {{ Form::label(null, 'Signed in as:  ')}}
                    {{ Auth::user()->username }}
                    {{ link_to_route('user.logout', "Sign out", null,  array('class'=>'btn btn-danger') )}}
                </form>
                @else
                {{ Form::open(array('url' => secure_url('user/login'), 'class' => "navbar-form navbar-right")) }}
                {{ Form::label('username', 'Username:  ')}}
                {{ Form::text('username') }}
                {{$errors->first('username') }}
                {{ Form::label('password', 'Password:  ') }}
                {{ Form::password('password') }}
                {{ $errors->first('password') }}
                {{ Form::submit('Log In', ['class' => 'btn btn-primary'])}}
                {{ link_to_route('user.create', "Sign Up", null,  array('class'=>'btn btn-success'))}}
                {{ Form::close() }}
                @endif
                
                {{ Form::open(array('url' => secure_url('user/search'), 'class' => "navbar-form navbar-right col-md-2")) }}
                    <!--<input type="text" onkeyup="search(this.value)" placeholder="Search For a User" class = "form-contrlol searchBar"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> -->
                    <div class="input-group searchContainer">
                        <span class="input-group-addon"><i class="ico-mglass"></i></span>
                        <input type="text" onkeyup="search(this.value)" class="form-control searchBar" placeholder="Search for a user" id="searchBar" name = "searchBar">
                        <div id="results" class ="arrow_box" >
                {{ Form::close() }}
                        {{ $hasResults = false }}
                            @if(Session::has('users'))
                                @foreach([Session::get('users')] as $user)
                                    @foreach($user as $userMatch)
                                        {{ Form::open(['url' => secure_url('add_friend')])}}
                                        {{ Form::input('text', 'adder', Auth::User()->id, ['hidden' => 'true'] ) }} 
                                        {{ Form::input('text', 'added', $userMatch->id, ['hidden' => 'true'] ) }} 
                                        <div class='match'> 
                                            {{ Form::submit('Add Friend')}}
                                            {{$userMatch->username}}
                                        </div>
                                        {{ Form::close()}}
                                        
                                        <?php $hasResults = true ?>
                                    @endforeach
                                @endforeach
                            @endif
                            
                            @if(!$hasResults)
                                <h4>No Results Found.</h4>
                            @endif
                        </div> 
                    </div>

        </nav>


    <!--SOME UGLY ASS CODE FOR MATRIX EFFECT-->
  <div class="matrixSymbol t1" style="left:2%;">*1 3 o*</div> <div class="matrixSymbol t10" style="left:20%;">** 13nv</div> <div class="matrixSymbol t1" style="left:60%;">*1 3 o* 13nv</div> <div class="matrixSymbol t6" style="left:5%;"> s oi 213 %$</div> <div class="matrixSymbol t2" style="left:22%;"> & @ # h $</div> <div class="matrixSymbol t7" style="left:78%;"> & @ # h ds oi 213 %$</div> <div class="matrixSymbol t3" style="left:12%;">P# G& PPs</div> <div class="matrixSymbol t8" style="left:34%;">P as D# </div> <div class="matrixSymbol t3" style="left:92%;">P as D# G& PPs</div> <div class="matrixSymbol t9" style="left:17%;"> }[/ mn</div> <div class="matrixSymbol t4" style="left:39%;">d9 n</div> <div class="matrixSymbol t10" style="left:81%;">d9 (1& }[/ mn</div> <div class="matrixSymbol t5" style="left:27%;">+ lks iu ~</div> <div class="matrixSymbol t7" style="left:49%;">++ -9 7n ~</div> <div class="matrixSymbol t5" style="left:61%;">++ -9 7nfs lks iu ~</div> <div class="matrixSymbol t1" style="left:6%;">$%sw</div> <div class="matrixSymbol t8" style="left:10%;">J w</div> <div class="matrixSymbol t1" style="left:50%;">$% J Asw</div> <div class="matrixSymbol t2" style="left:7%;">b9   1pasodf 8 as</div> <div class="matrixSymbol t5" style="left:82%;">b157  ajs</div> <div class="matrixSymbol t2" style="left:28%;">b157  aj 139   1pasodf 8 as</div> <div class="matrixSymbol t3" style="left:22%;">g 7 j yh</div> <div class="matrixSymbol t7" style="left:34%;">0 s9s h</div> <div class="matrixSymbol t3" style="left:42%;">0 s9s 8 g 7 j yh</div> <div class="matrixSymbol t4" style="left:77%;">2 j j 22h  j</div> <div class="matrixSymbol t6" style="left:59%;"> 9 ijo j 8 2 j</div> <div class="matrixSymbol t4" style="left:41%;"> 9 ijo j 8 2 j j 22h  j</div> <div class="matrixSymbol t1" style="left:23%;">*1 13nv</div> <div class="matrixSymbol t9" style="left:22%;">*1 v</div> <div class="matrixSymbol t1" style="left:12%;">*1 3 o* 13nv</div> <div class="matrixSymbol t2" style="left:56%;"> & @oi 213 %$</div> <div class="matrixSymbol t10" style="left:87%;"> & @ #  %$</div> <div class="matrixSymbol t2" style="left:72%;"> & @ # h ds oi 213 %$</div> <div class="matrixSymbol t3" style="left:34%;">P as  PPs</div> <div class="matrixSymbol t7" style="left:74%;">P G</div> <div class="matrixSymbol t3" style="left:16%;">P as D# G& PPs</div> <div class="matrixSymbol t4" style="left:19%;">d9  mn</div> <div class="matrixSymbol t6" style="left:84%;"> }[/ mn</div> <div class="matrixSymbol t4" style="left:75%;">d9 (1& }[/ mn</div> <div class="matrixSymbol t5" style="left:39%;">++ -9 7nfks iu ~</div> <div class="matrixSymbol t8" style="left:02%;">++ -9 7n iu ~</div> <div class="matrixSymbol t5" style="left:38%;">++ -9 iu ~</div> <div class="matrixSymbol t1" style="left:60%;">$% J Asw</div> <div class="matrixSymbol t9" style="left:37%;">$% w</div> <div class="matrixSymbol t1" style="left:94%;">J Asw</div> <div class="matrixSymbol t2" style="left:48%;">b157  aj  8 as</div> <div class="matrixSymbol t10" style="left:47%;">bj 139   1pasodf 8 as</div> <div class="matrixSymbol t2" style="left:76%;">b157  aj 139   1pasodf 8 as</div> <div class="matrixSymbol t3" style="left:88%;">0 s9s 8 gh</div> <div class="matrixSymbol t8" style="left:91%;">0 s 7 j yh</div> <div class="matrixSymbol t3" style="left:64%;">0 s9s 8g 7 j yh</div> <div class="matrixSymbol t4" style="left:27%;"> 9 ijo j 8 2  j</div> <div class="matrixSymbol t9" style="left:95%;"> 92 j j 22h  j</div> <div class="matrixSymbol t4" style="left:14%;"> 9 ijo j 8 2 j j 22h  j</div>
            
        

        <div class="row" >
            <div class="col-md-2 hidden-sm hidden-xs">
                @if(Session::has('created'))
                    <div class="well" style = "z-index:2;position: absolute;min-height:400px; min-width:400px;">
                        <h2>{{ Session::get('created') }}  : Account Created </h2>
                        <h4>Please Sign in</h4>
                    </div>
                @endif                
                
                @yield('left')
            </div>
              
            <div class="col-md-8"> 
                @yield('body')
            </div>
              
            <div class="col-md-2"> 
                @yield('right')
            </div>
  
        </div>



        
          
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {{ HTML::script('jquery-2.1.3.min.js') }}
        {{ HTML::style('bootstrap.min.css') }}
        {{ HTML::style('stylesheet.css') }}

        <script type="text/javascript" src="bootstrap.min.js"></script>
        
    </body>
</html>