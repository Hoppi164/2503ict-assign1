@extends('layouts.layout')


@section('left')
<!--CreatePost Form-->

<div class="panel panel-info newPost">
    <div class="panel-heading"><div class="panel-title">Make Post</div></div>
    <div class = "panel-body well well-sm">
        {{ Form::open(['url' => secure_url('add_item_action'), 'class' => "form-horizontal", 'files'=>true]) }}
        {{ Form::input('text', 'name', Auth::user()->username, ['hidden' => 'true'] ) }} 
        {{ Form::label('title', 'Title:  ')}}
        {{ Form::text('title') }}
        {{ Form::label('post', 'Post:  ')}}
        {{ Form::textarea('post', null, ['rows' => 4, 'class' => 'form-control', 'style' => 'resize:vertical; max-height:300px;']) }}
        <input class="form-control" type="file" name="image" id="image" />
        {{ Form::label('privacy', 'Public:  ')}}
        {{ Form::radio('privacy', 'public') }}<br>
        {{ Form::label('privacy', 'Friends Only:  ')}}
        {{ Form::radio('privacy', 'friends', true) }}        
        {{ Form::submit('Submit', ['class' => "form-control"])}}
        {{ Form::close()}}
    </div>
</div>
<!--endCreatePostForm-->
@stop




@section('body')
    <div class = "well wall">
        @if ($threads)
            @foreach ($threads as $thread)
                <!--POST-->
                <div class = "panel panel-primary">
                    <div class="panel-heading">{{{ $thread->title }}}</div>
                    <div class="panel-body">
                        <div class = "col-md-2">
                            <?php $image = get_ProfileImage($thread->userID) ?>
                            <a href="#" class="thumbnail"> {{ HTML::image("images/$image") }} </a>
                            {{{ get_User($thread->userID) }}}
                        </div>
                        <div class = "col-md-10 panel">
                            {{{ $thread->post }}}
                                <a class="thumbnail" >{{ HTML::image("images/$thread->filename")}}</a>
                        </div>
                    </div>
                    @if( Auth::User()->id == $thread-> userID)
                        <a class="btn btn-danger" href="{{{ url("deletePost/$thread->id") }}}">Delete Post</a>
                        <a class="btn btn-success" href="{{{ url("update/$thread->id") }}}">Edit Post</a>
                    @endif
                    <div class = "panel-group">
                        <!--COMMENT FORM-->
                        <div class = "panel panel-info">
                            <div class="panel-heading showBtn"><h4 class="panel-title"><a data-toggle="collapse" data-target="#commentForm{{{ $thread->id }}}" class = "collapsed">New Comment</a></h4></div>
                            <div id="commentForm{{{ $thread->id }}}" class="panel-collapse collapse">
                                <div class="panel-body comment-body">
                                    {{ Form::open(['url' => secure_url('add_comment_action'), 'class' => "form-horizontal", 'files'=>true]) }}
                                    {{ Form::input('text', 'name', Auth::user()->username, ['hidden' => 'true'] ) }} 
                                    {{ Form::input('text', 'threadId', $thread->id, ['hidden' => 'true'] ) }} 
                                    {{ Form::label('comment', 'Comment:  ')}}
                                    {{ Form::textarea('comment', null, ['rows' => 4, 'class' => 'form-control', 'style' => 'resize:vertical; max-height:300px;']) }}
                                    <input class="form-control" type="file" name="image" id="image" />
                                    {{ Form::submit('Submit', ['class' => "btn btn-success form-control"])}}
                                    {{ Form::close()}}
                                </div>
                            </div>
                        </div>
                        <!--END COMMENT FORM-->
                        <!--COMMENTS-->
                        <div class = "panel panel-default">
                            <div class="panel-heading showBtn"><h4 class="panel-title"><a data-toggle="collapse" data-target="#collapseContainer{{{ $thread->id }}}"  class = "collapsed">View Comments ( {{{count(get_comments($thread->id))}}} )</a></h4></div>
                            <div id="collapseContainer{{{ $thread->id }}}" class="panel-collapse collapse">
                                <div class="panel panel-default" id="comment">
                                    @foreach(get_comments($thread->id) as $comment)
                                        <div class="panel panel-success" ><div class ="panel-heading">{{{ get_User($comment->userID)}}} </div></div>
                                        <div class="panel panel-body commentBody">
                                            <p class="col-md-6">{{{ $comment->comment}}}</p>
                                            <a class="thumbnail" >{{ HTML::image("images/$comment->filename")}}</a>
                                        </div>
                                        @if($comment-> userID == Auth::User()->id)
                                            <a class="btn btn-danger" href="{{{ url("deleteComment/$comment->id") }}}">Remove Comment</a>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!--END COMMENTS-->
                        
                    </div>
                </div>
                <!--END OF POST-->
                
            @endforeach
        @else
            <p>No items found.</p>
        @endif    
        
    </div>

@stop

@section('right')
    <div class="panel panel-info" id="profilePanel">
        <div class="panel-heading"><div class="panel-title"><a>{{{Auth::user()->username}}}</a></div></div>
        <div class = "panel-body well well-sm">
            <?php $image = Auth::user()->profileImage ?>
            <a href="#" class="thumbnail"> {{ HTML::image("images/$image")}}</a>
            <h4>{{{Auth::user()->DOB}}}</h4>
            <h4>{{{Auth::user()->email}}}</h4>
            {{ link_to_route('user.edit', "Edit Profile", null,  ['class'=>'btn btn-success'])}}
            
            <div class = "panel panel-default">
                <h3>Friends:</h3>
                @foreach(get_friends(Auth::User()->id) as $friend)
                    
                    <?php 
                    $name ="";
                    if($friend-> adderID != Auth::User()-> id){
                      $name = get_User($friend-> adderID);  
                      $friend = $friend->adderID;
                    } 
                    elseif($friend-> addedID != Auth::User()-> id){
                      $name = get_User($friend-> addedID);  
                      $friend = $friend->addedID;
                    } 
                    ?>
                        <div class="well well-sm">
                            <a href="{{{secure_url('user/profile', $friend)}}}" class = "btn btn-default col-md-6">{{{$name}}}</a>
                            <a href = "{{{secure_url('user/unfriend', $friend)}}}" class = "btn btn-danger col-md-6">Unfriend</a>
                            <?php $image = get_ProfileImage($friend) ?>
                            <a href="{{{secure_url('user/profile', $friend)}}}" class="thumbnail"> {{ HTML::image("images/$image")}}</a>
                        </div>
                        
                @endforeach
            </div>
    </div>
@stop