@extends('layouts.layout')


@section('left')
<div class = "well pull-left signUpForm">
    <h2>Create New User:</h2>
    {{ Form::open(array('url' => secure_url('user'))) }}
        {{ Form::label('username', 'Username:')}}
        {{ Form::text('username','',array('required' => 'required')) }}
        {{ $errors->first('username') }}
        <p></p>
        {{ Form::label('email', 'Email:')}}
        {{ Form::email('email','',array('required' => 'required')) }}
        {{ $errors->first('email') }}
        <p></p>
        {{ Form::label('password', 'Password:')}}
        {{ Form::password('password','',array('required' => 'required')) }}
        {{ $errors->first('password') }}
        <p></p>
        <p></p>
        {{ Form::label('DOB', 'Date of Birth:')}}
        {{ Form::input('date', 'DOB', null, ['required' => 'required']) }}
        {{ $errors->first('DOB') }}
        <p></p>
        {{ Form::submit('Submit' , ['class' => 'btn btn-primary']) }}
    {{ Form::close() }}
</div>


@stop