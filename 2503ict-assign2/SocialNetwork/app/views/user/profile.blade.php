@extends('layouts.layout')


@section('left')
        <div class = "panel panel-default">
            <div class = "panel-heading"> <h5>{{{ $user->username }}}'s Friends:</h5> </div>
            <div class = "panel-body">
                @foreach(get_friends($user->id) as $friend)
                    <?php 
                        $name ="";
                        if($friend-> adderID != Auth::User()-> id){
                          $name = get_User($friend-> adderID);  
                          $friend = $friend->adderID;
                        } 
                        elseif($friend-> addedID != Auth::User()-> id){
                          $name = get_User($friend-> addedID);  
                          $friend = $friend->addedID;
                    } 
                    ?>
                        <div class="well well-sm">
                            <a href="{{{secure_url('user/profile', $friend)}}}" class = "btn btn-default col-md-6">{{{$name}}}</a>
                            @if(Auth::User()->id == $user->id)
                                <a href = "{{{secure_url('user/unfriend', $friend)}}}" class = "btn btn-danger col-md-6">Unfriend</a>
                            @endif
                            <?php $image = get_ProfileImage($friend) ?>
                            <a href="{{{secure_url('user/profile', $friend)}}}" class="thumbnail"> {{ HTML::image("images/$image")}}</a>
                        </div>
                @endforeach
            </div>
        </div>

@stop




@section('body')
    <div class="panel panel-info" id="profilePanel">
        <div class="panel-heading"><div class="panel-title"><a> {{{ $user->username }}} </a></div></div>
        <div class = "panel-body well well-sm">
            <div class="col-md-6"><a href="#" class="thumbnail"> {{ HTML::image("images/$user->profileImage")}}</a></div>
            <div class="col-md-6">
                <h2>Name: {{{ $user->username }}}</h2>
                <h2>AGE: {{{ $user-> DOB }}}</h2>
                <h2>Email: {{{ $user->email }}}</h2>
            </div>
    </div>
@stop

@section('right')
@stop