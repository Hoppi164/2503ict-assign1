@extends('layouts.layout')


@section('left')
        <div class = "panel panel-default">
            <div class = "panel-heading"> <h5>{{{ Auth::User()->username }}}'s Friends:</h5> </div>
            <div class = "panel-body">
                @foreach(get_friends(Auth::User()->id) as $friend)
                    <?php 
                        $name ="";
                        if($friend-> adderID != Auth::User()-> id){
                          $name = get_User($friend-> adderID);  
                          $friend = $friend->adderID;
                        } 
                        elseif($friend-> addedID != Auth::User()-> id){
                          $name = get_User($friend-> addedID);  
                          $friend = $friend->addedID;
                    } 
                    ?>
                        <div class="well well-sm">
                            <a href="{{{secure_url('user/profile', $friend)}}}" class = "btn btn-default col-md-6">{{{$name}}}</a>
                            @if(Auth::User()->id == Auth::User()->id)
                                <a href = "{{{secure_url('user/unfriend', $friend)}}}" class = "btn btn-danger col-md-6">Unfriend</a>
                            @endif
                            <?php $image = get_ProfileImage($friend) ?>
                            <a href="{{{secure_url('user/profile', $friend)}}}" class="thumbnail"> {{ HTML::image("images/$image")}}</a>
                        </div>
                @endforeach
            </div>
        </div>
@stop




@section('body')
    <div class="panel panel-info" id="profilePanel">
        <div class="panel-heading"><div class="panel-title"><a>{{ Auth::user()->username }}</a></div></div>
        <div class = "panel-body well well-sm">
            <?php $image = Auth::user()->profileImage ?>
            <div class="col-md-6"><a href="#" class="thumbnail"> {{ HTML::image("images/$image")}}</a></div>
            <div class="col-md-6">
                {{ Form::open(['url' => secure_url('update_user_action'), 'class' => "form-horizontal", 'files'=>true]) }}
                {{ Form::input('text', 'id', Auth::user()->id, ['hidden' => 'true'] ) }} 
                {{ Form::input('text', 'oldImage', Auth::user()->profileImage, ['hidden' => 'true'] ) }} 
                {{ Form::label('name', 'Name:  ')}}
                {{ Form::input('text', 'name', Auth::user()->username) }} <br>
                {{ Form::label('DOB', 'Date Of Birth:  ')}}
                {{ Form::input('text', 'DOB', Auth::user()->DOB) }} <br>
                {{ Form::label('email', 'Email Address:  ')}}
                {{ Form::input('text', 'email', Auth::user()->email) }} <br>
                {{ Form::label('image', 'profileImage:  ')}}
                <input class="form-control" type="file" name="image" id="image" /><br>

                {{ Form::submit('update')}}
                {{ Form::close() }}

            </div>
    </div>
@stop

@section('right')
@stop