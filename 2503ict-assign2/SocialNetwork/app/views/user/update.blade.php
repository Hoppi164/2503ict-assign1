@extends('layouts.layout')



@section('body')
<!--updateForm-->
<div class = "panel panel-primary">

    {{ Form::open(['url' => secure_url('update_item_action'), 'class' => "form-horizontal", 'files'=>true]) }}
    {{ Form::input('text', 'name', Auth::user()->username, ['hidden' => 'true'] ) }} 
    
    {{ Form::input('text', 'threadId', $thread->id, ['hidden' => 'true'] ) }} 
    {{ Form::input('text', 'oldImage', $thread->filename, ['hidden' => 'true'] ) }} 
    
    {{ Form::label('title', 'Title:  ')}}
    {{ Form::text('title', $thread->title) }}
    {{ Form::label('post', 'Post:  ')}}
    {{ Form::textarea('post', null, ['rows' => 4, 'class' => 'form-control', 'style' => 'resize:vertical; max-height:300px;']) }}
    <input class="form-control" type="file" name="image" id="image" />
    {{ Form::submit('Submit', ['class' => "form-control"])}}
    {{ Form::close()}}
    
    <br>
    <br>
    <br>
    <br>
    <br>


    
    
    <!--<form method="post" action="{{{ url('update_item_action') }}}" enctype="multipart/form-data">-->
    <!--    <input type="hidden" name="id" value="{{{ $thread->id }}}"> -->
    <!--    <input type="hidden" name="filename" value="{{{ $thread->filename }}}"> -->
    <!--    <div class="panel-heading">-->
    <!--        <input type="text" class="form-control" name="title" id="title" value="{{{ $thread->title }}}">-->
    <!--    </div>-->
        
    <!--    <div class="post">-->
    <!--        <div class = "col-md-2">-->
    <!--            <a href="#" class="thumbnail" ><img src="{{{ url("images/smile.png") }}}" class = "displayPic"></a>-->
    <!--        </div>-->
    <!--        <div class = "col-md-10 panel">-->
    <!--            <textarea class="form-control" rows="4"  name="post" id="post">{{{ $thread->post }}}</textarea>-->
    <!--            <input class="form-control" type="file" name="image" id="image" value=""> -->
    <!--        </div>-->
    <!--    <a href="/2503ict-assign1/public/home" class ="btn btn-danger">Cancel</a>-->
    <!--    <input class="btn btn-success" type="submit" value="Submit"/>-->
    <!--    </div>-->
    <!--</form>-->
</div>




@stop
