<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/', function()//Route to login page
{
    return View::make('user.login');
});

Route::get('home', function()//Route to Home page
{
	if(Auth::User()){
		$user = Auth::User()->id;
	    $threads = get_threads($user);
	}
	else{//Future code for if not logged in
		$user = -999;
	    $threads = get_threads($user);
	}
    return View::make('user.home')->withThreads($threads);
});
//Route redirects to controllers
Route::post('user/search', array('as' => 'user.search', 'uses' => 'UserController@search'));//Search
Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));//LOGIN
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));//LOGOUT
Route::get('user/profile/{Id}', array('as' => 'user.profile', 'uses' => 'UserController@profile'));//VIEWPROFILE
Route::get('user/edit', array('as' => 'user.edit', 'uses' => 'UserController@edit'));//EDITPAGE
Route::get('user/unfriend/{Id}', array('as' => 'user.unfriend', 'uses' => 'UserController@unfriend'));//UNFRIEND METHOD
Route::resource('user', 'UserController'); //UNIVERSAL RESOURCE THAT DOESNT SEEM TO WORK

//getters
function get_threads($user)//GETS ALL THREADS THAT ARE PUBLIC, MADE BY THE USER OR BY USERS FRIENDS
{
	$sql = "SELECT * FROM threads 
	WHERE privacy = 'public'
	OR userID = ?
	OR userID IN (SELECT adderID FROM friends WHERE adderID = ?) 
	OR userID IN (SELECT addedID FROM friends WHERE addedID = ?)
	ORDER BY id DESC";
	//THIS DOES NOT WORK PROPERLY

	$threads = DB::select($sql, [$user, $user, $user]  );
	return $threads;
}
function get_thread($id)//GETS SPECIFIC THREAD BY ID
{
	$sql = "select * from threads where id = ?";
	$threads = DB::select($sql, array($id));
// If we get more than one item or no items display an error
	if (count($threads) != 1) 
	{
    die("Invalid query or result: $sql\n");
  	}
	// Extract the first item (which should be the only item)
	$thread = $threads[0];
	return $thread;
}
function get_friends($id){//GET ALL FRIENDS OF USER
	$sql = "SELECT adderID, addedID FROM friends where addedID = ? OR adderID =?";
	$friends = DB::select($sql, [$id, $id] );
	return $friends;
}


function get_comments($id){//GET ALL COMMENTS FOR THREAD
	$sql = "select * from comments where comments.threadId = ?";
	$comments = DB::select($sql, array($id));
	return $comments;
}

function get_ID($username)//GET ID OF USERNAME
{
    $sql = "select id from users where username = ?";
    $ID = DB::select($sql,  array($username));
    return $ID[0]->id;
}

function get_User($ID)//GET NAME OF USER
{
  $sql = "select username from users where id = ?";
  $user = DB::select($sql,  array($ID));
  return $user[0]->username;
}
function get_UserDATA($ID)//GET ALL DATA OF USER
{
  $sql = "select username, DOB, email, profileImage, id from users where id = ?";
  $user = DB::select($sql,  [$ID]);
  return $user;
}
function get_Email($ID)//GET EMAIL OF USER
{
  $sql = "select email from users where id = ?";
  $user = DB::select($sql,  array($ID));
  return $user[0]->email;
}
function get_DOB($ID)//GET DATE OF BIRTH OF USER
{
  $sql = "select DOB from users where id = ?";
  $user = DB::select($sql,  array($ID));
  return $user[0]->DOB;
}
function get_ProfileImage($ID)//GET PROFILEIMAGE OF USER
{
  $sql = "select profileImage from users where id = ?";
  $user = DB::select($sql,  array($ID));
  return $user[0]->profileImage;
}

function get_Users()//GET ALL USERS
{
  $sql = "select username from users";
  $users = DB::select($sql);
  return $users;
}

Route::get('update/{id}', function($id)//ROUTE TO UPDATE
{
	$thread = get_thread($id);
	return View::make('user.update')->withThread($thread);
});

//setters

Route::get('deletePost/{id}', function($id)//DELETES POST AND REDIRECTS TO HOME
{
	$sql = "delete from threads where threads.id = ?";
	DB::delete($sql,(array)$id);
	$sql = "delete from comments where comments.threadId = ?";//DELETES ALL RELATED COMMENTS
	DB::delete($sql,(array)$id);
    return Redirect::to('home');
});
Route::get('deleteComment/{id}', function($id)//DELETES COMMENT AND REDIRECTS TO HOME
{
	$sql = "delete from comments where comments.id = ?";
	DB::delete($sql,(array)$id);
    return Redirect::to(url("home"));
});



Route::post('add_item_action', function()//ROUTE TO ADD NEW POST
{
	$name = Input::get('name');
	$post = Input::get('post');
	$title = Input::get('title');
	$privacy = Input::get('privacy');
	$file = Input::file('image');// gerts file
	$destinationPath = 'images/';// gets file path
	$filename="";
	if ($file){
		$filename = $file->getClientOriginalName();//gets filename
		Input::file('image')->move($destinationPath, $filename);//saves file to server
	} 
	$ID = get_ID($name);
	$threadID = add_item($ID, $title, $post, $privacy, $filename);//adds all data to database
	// If successfully created then display newly created item
	if ($threadID) 
	{
	    $threads = get_threads($ID);
	    return Redirect::to(url("home"));
	} 
	else { die("Error adding item"); }
});

function add_item($ID, $title, $post, $privacy, $filename)//ACTUALLY ADDS NEW POST
{

	$sql = "insert into threads (userID, title, post, privacy, filename) values (?, ?, ?, ?, ?)";
	
	DB::insert($sql, array($ID, $title, $post, $privacy, $filename));
	
	$id = DB::getPdo()->lastInsertId();
	
	return $id;
}

Route::post('update_item_action', function()//ROUTE TO UPDATE POST
{
	$name = Input::get('name');
	$post = Input::get('post');
	$title = Input::get('title');
	$file = Input::file('image');// gerts file
	$destinationPath = 'images/';// gets file path
	$filename=Input::get('oldImage');
	$ID = Input::get('threadId');
	if ($file){
		$filename = $file->getClientOriginalName();
		Input::file('image')->move($destinationPath, $filename);
	} 
	$sql = "update threads set title = ?, post = ?, filename = ? where id = ?";
	DB::update($sql, array($title, $post, $filename, $ID));
    return Redirect::to(url("home"));
});

Route::post('add_comment_action', function()//ROUTE TO ADD NEW COMMENT
{
	$name = Input::get('name');
	$comment = Input::get('comment');
	$threadId = Input::get('threadId');
	$file = Input::file('image');
	$destinationPath = 'images/';
	$filename="";
	if ($file){
		$filename = $file->getClientOriginalName();
		Input::file('image')->move($destinationPath, $filename);
	} 
	$user = Auth::user()->id;
	$id = add_comment($threadId, $user, $comment, $filename);
	// If successfully created then display newly created item
	if ($id) 
	{
	    return Redirect::to(url("home"));
	} 
	else
	{
		die("Error adding item");
	}
});
function add_comment($threadId, $user, $comment, $filename) //ACTUALLY ADDS NEW COMMENT
{
	$sql = "insert into comments (threadId ,userID, comment, filename) values (?, ?, ?, ?)";
	
	DB::insert($sql, array($threadId, $user, $comment, $filename));
	
	$id = DB::getPdo()->lastInsertId();
	
	return $id;
}

Route::post('update_user_action', function(){//ROUTE TO UPDATE USER DATA
	$id = Input::get('id');
	$name = Input::get('name');
	$DOB = Input::get('DOB');
	$email = Input::get('email');
	$file = Input::file('image');
	$destinationPath = 'images/';
	$filename=Input::get('oldImage');
	if ($file){
		$filename = $file->getClientOriginalName();
		Input::file('image')->move($destinationPath, $filename);
	}
	$sql = "update users set username = ?, DOB = ?, email = ?, profileImage=? where id = ?";
	DB::update($sql, array($name, $DOB, $email, $filename, $id));
    return Redirect::to(url("home"));
	
});
Route::post('add_friend', function(){//ROUTE TO ADD NEW FRIEN
	$adder = Input::get('adder');
	$added = Input::get('added');
	$sql ="INSERT INTO friends (adderID, addedID) VALUES ( ?, ? )";
	DB::insert($sql, [$adder, $added]);
    return Redirect::to(url("home"));
});


